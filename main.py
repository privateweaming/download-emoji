#!/usr/bin/env python
# coding: utf-8
"""
Author       : weaming
Created Time : 2017-01-13 15:14:19

File Name    : main.py
Description  :
    Download emoji pictures from http://www.webpagefx.com/tools/emoji-cheat-sheet/
"""
import re, json
import os
import requests
import shutil

results = {}
target = 'emojis'

def func(page):
    p = re.compile(r'.*?data-src="(.+?emojis/([\w\-]+\.png))".+?class="name".*?>(\w+)<', re.S)
    result = p.findall(page)
    if not result:
        return

    title = re.search(r'(\w+)</h2>', page)
    title = title and title.group(1).lower()

    print(title, len(result))

    emoji = []
    for x in result:
        emoji.append({'url': x[0], 'name': x[1], 'code': x[2]})
    results[title] = emoji
    return title, emoji

def download():
    for title, li in results.items():
        dir_path = os.path.join(target, title)
        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)
        for e in li:
            url = 'http://www.webpagefx.com/tools/emoji-cheat-sheet/' + e['url']
            name = e['name']
            out_path = os.path.join(dir_path, name)
            if os.path.isfile(out_path):
                continue
            print('Downloading: %s/%s' % (title, name))
            resp = requests.get(url, stream=True)
            if resp.status_code == 200:
                with open(out_path, 'wb') as out:
                    shutil.copyfileobj(resp.raw, out)
            del resp

if __name__ == '__main__':
    with open('emoji.html', 'r') as f:
        page = f.read()

    map(func, page.split('<h2>'))
    with open('emojis.json', 'w') as f:
        json.dump(results, f, ensure_ascii=False, indent=4)

    download()
